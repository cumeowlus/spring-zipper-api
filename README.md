# Spring Zipper API

This is a simple demo project, it archives into a single .zip file multiple given files

## Usage

Endpoint : `/zipper`
Files must passed as form-data.
This can achieved used Postman via Body -> Form-data -> Naming a key "files" and setting it at as a file.

## Code adapting

### Multipe archive types

The backend is built with such design in mind.

* A type parameter can be added to the endpoint.
* New classes extending Archiver (which would become abstract) for each type
* The controller method can be modified to dynamically change the HTTP response content-type.

### Increased request count suggestions

* Write archive to memory to act as a buffer and avoid overloading the server's allocated memory.
* Load balancing between multiple instances of the backend. Requests are redirected to instances with the lowest load.
* If upscaling is not possible, limit the amount of parallel requests, but at the cost of user response time

### Increasing upload size

* Can be modified with `spring.servlet.multipart.max-request-size` in `application.properties`