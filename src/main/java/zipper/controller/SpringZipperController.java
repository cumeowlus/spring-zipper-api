package zipper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import zipper.service.MetricsService;
import zipper.service.SpringZipperService;
import zipper.utils.Archiver;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@RestController
public class SpringZipperController {

    private final SpringZipperService zipperService;
    private final MetricsService metricsService;

    @Autowired
    public HttpServletRequest httpServletRequest;

    @Autowired
    public SpringZipperController(SpringZipperService zipperService, MetricsService metricsService) {
        this.zipperService = zipperService;
        this.metricsService = metricsService;
    }

    @GetMapping("/zipper")
    public ResponseEntity<Resource> archive(@RequestParam List<MultipartFile> files) throws IOException {
        metricsService.recordRequest(httpServletRequest.getRemoteAddr());
        MediaType contentType = MediaType.valueOf("application/zip"); // different archive types can be added as request param in the future
        return ResponseEntity.ok()
                .contentType(contentType)
                .body(zipperService.getArchiveFile(files, Archiver.ArchiveType.ZIP));
    }
}
