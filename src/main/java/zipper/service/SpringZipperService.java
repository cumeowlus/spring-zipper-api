package zipper.service;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.UnsupportedMediaTypeStatusException;
import zipper.utils.Archiver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class SpringZipperService {

    public ByteArrayResource getArchiveFile(List<MultipartFile> files, Archiver.ArchiveType archiveType) throws IOException {
        ArrayList<MultipartFile> filesArrayList = new ArrayList<>(files);
        ByteArrayResource archiveFileBytes;

        switch (archiveType) {
            case ZIP:
                archiveFileBytes = Archiver.getZipArchive(files);
                break;
            default:
                throw new UnsupportedMediaTypeStatusException("Archive type is not supported");
        }

        return archiveFileBytes;

    }
}
