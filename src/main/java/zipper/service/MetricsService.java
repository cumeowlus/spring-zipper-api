package zipper.service;

import org.springframework.stereotype.Service;
import zipper.model.Record;
import zipper.repository.MetricsRepository;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class MetricsService {

    private final MetricsRepository repository;

    public MetricsService(MetricsRepository repository) {
        this.repository = repository;
    }

    public void recordRequest(String remoteAddr) {
        if (remoteAddr.isEmpty() || remoteAddr == null) {
            throw new RuntimeException("IP Address is empty or null");
        }
        String todayAsString = new SimpleDateFormat("ddMMyyyy").format(new Date());
        Record record = repository.findByIpAddrAndDateString(remoteAddr, todayAsString);
        if (record != null) {
            record.setRequestCount(record.getRequestCount() + 1);
        } else {
            record = new Record();
            record.setIpAddr(remoteAddr);
            record.setDateString(todayAsString);
            record.setRequestCount(1);
        }
        repository.save(record);
    }
}
