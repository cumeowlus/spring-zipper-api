package zipper.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import zipper.model.Record;
import zipper.model.RecordId;

@Repository
public interface MetricsRepository extends CrudRepository<Record, RecordId> {
    Record findByIpAddrAndDateString(String ip, String date);
}
