package zipper.utils;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Archiver {
    public enum ArchiveType {
        ZIP, RAR, TAR, SEVENZ
    }

    public static ByteArrayResource getZipArchive(List<MultipartFile> files) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
        ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);

        for (MultipartFile f : files) {
            zipOutputStream.putNextEntry(new ZipEntry(f.getOriginalFilename()));
            zipOutputStream.write(f.getBytes());
        }

        IOUtils.closeQuietly(zipOutputStream);
        IOUtils.closeQuietly(bufferedOutputStream);
        IOUtils.closeQuietly(byteArrayOutputStream);

        // Convert output stream to ByteArrayResource for end point response
        ByteArrayResource resource = new ByteArrayResource(byteArrayOutputStream.toByteArray());
        return resource;

    }
}
