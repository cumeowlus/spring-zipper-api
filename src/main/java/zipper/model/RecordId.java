package zipper.model;

import java.io.Serializable;

public class RecordId implements Serializable {
    private String ipAddr;
    private String dateString;

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }
}
