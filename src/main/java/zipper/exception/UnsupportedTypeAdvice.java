package zipper.exception;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.server.UnsupportedMediaTypeStatusException;

@ControllerAdvice
public class UnsupportedTypeAdvice {
    @Value("${spring.servlet.multipart.max-request-size}")
    private String maxFileSize;

    @ExceptionHandler(UnsupportedMediaTypeStatusException.class)
    public ResponseEntity<String> handleMaxUploadSizeExceededException() {
        String errorMessage = "Unsupported file type. Supported type is .zip";
        return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
                .contentType(MediaType.TEXT_PLAIN)
                .body(errorMessage);
    }
}
