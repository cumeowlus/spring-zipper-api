package zipper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringZipperApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringZipperApplication.class, args);
    }
}
