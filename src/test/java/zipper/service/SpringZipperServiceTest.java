package zipper.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.UnsupportedMediaTypeStatusException;
import zipper.utils.Archiver;

import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SpringZipperServiceTest {

    private SpringZipperService springZipperService;

    private List<MultipartFile> files;
    private MockMultipartFile fileA;
    private MockMultipartFile fileB;

    @BeforeEach
    public void setup() {
        springZipperService = new SpringZipperService();
        fileA = new MockMultipartFile("files", "testA.txt", MediaType.TEXT_PLAIN_VALUE, "testtextA".getBytes());
        fileB = new MockMultipartFile("files", "testB.txt", MediaType.TEXT_PLAIN_VALUE, "testtextB".getBytes());
        files = List.of(fileA, fileB);
    }

    @Test
    void getArchiveFileTest_notnull() throws IOException {
        // given files and archive type
        Archiver.ArchiveType archiveType = Archiver.ArchiveType.ZIP;

        // when getArchiveFile is called
        ByteArrayResource result = springZipperService.getArchiveFile(files, archiveType);

        // return a non null ByteArrayResource
        assertNotNull(result);
        assertTrue(result.getByteArray().length > 0);
    }

    @Test
    void getArchiveFileTest_unsupportedType() throws IOException {
        // given files but incorrect type
        Archiver.ArchiveType archiveType = Archiver.ArchiveType.SEVENZ;
        // when getArchiveFile is called
        // then the backend throws an exception
        assertThrows(UnsupportedMediaTypeStatusException.class, () -> springZipperService.getArchiveFile(files, archiveType));
    }
}