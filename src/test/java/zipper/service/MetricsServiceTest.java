package zipper.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import zipper.repository.MetricsRepository;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class MetricsServiceTest {

    private MetricsService metricsService;

    @MockBean
    private MetricsRepository metricsRepository;

    @BeforeEach
    void setUp() {
        metricsService = new MetricsService(metricsRepository);
    }

    @Test
    public void recordRequestException() {
        assertThrows(RuntimeException.class, () -> metricsService.recordRequest(null));
        assertThrows(RuntimeException.class, () -> metricsService.recordRequest(""));
    }


}