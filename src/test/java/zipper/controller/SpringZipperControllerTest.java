package zipper.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.multipart.MultipartFile;
import zipper.service.MetricsService;
import zipper.service.SpringZipperService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class SpringZipperControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SpringZipperService zipperService;
    @MockBean
    private MetricsService metricsService;
    @Mock
    private HttpServletRequest mockedHttpServletRequest;

    private SpringZipperController controller;

    private List<MultipartFile> files;
    private MockMultipartFile fileA;
    private MockMultipartFile fileB;

    @BeforeEach
    public void setUp() {
        controller = new SpringZipperController(zipperService, metricsService);
        controller.httpServletRequest = mockedHttpServletRequest;
        fileA = new MockMultipartFile("files", "testA.txt", MediaType.TEXT_PLAIN_VALUE, "testtextA".getBytes());
        fileB = new MockMultipartFile("files", "testB.txt", MediaType.TEXT_PLAIN_VALUE, "testtextB".getBytes());
        files = List.of(fileA, fileB);
        when(mockedHttpServletRequest.getRemoteAddr()).thenReturn("1.1.1.1");
    }

    @Test
    public void verifyServiceArchiveTest() throws IOException {
        //when archive endpoint is called
        controller.archive(files);
        //then proper service method is called
        verify(zipperService).getArchiveFile(anyList(), any());
    }

    @Test
    public void verifyMetricsTest() throws IOException {
        //when archive endpoint is called
        controller.archive(files);
        //then proper metrics service method is called
        verify(metricsService).recordRequest(any());
    }

    @Test
    public void archiveIntegrationTest() throws Exception {
        mockMvc.perform(multipart("/zipper")
                        .file(fileA)
                        .file(fileB)
                        .with(request -> {
                            request.setMethod("GET");
                            return request;
                        }))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.valueOf("application/zip")));
    }
}